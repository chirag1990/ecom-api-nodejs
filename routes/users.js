const {User} = require('../models/user')
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

router.get(`/`, async (req, res) => {
    const userList = await User.find();

    if (!userList) {
        res.status(500).json({success: false})
    }

    res.send(userList);
});

router.post('/create', async (req, res) => {
    let user = new User({
        name: req.body.name,
        email: req.body.email,
        passwordHash: bcrypt.hashSync(req.body.password, 10),
        phone: req.body.phone,
        isAdmin: req.body.isAdmin,
        houseNumber: req.body.houseNumber,
        street: req.body.street,
        city: req.body.city,
        postCode: req.body.postCode,
        country: req.body.country
    })

    user = await user.save();

    if (!user) {
        return res.status(400).send({success: false, data: 'User cannot be created.'})
    } 

    res.send({success: true, data: user})
})

module.exports = router