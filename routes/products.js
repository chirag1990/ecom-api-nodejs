const {Product} = require('../models/product')
const express = require('express');
const { Category } = require('../models/category');
const mongoose = require('mongoose');
const router = express.Router();

router.get(`/`, async (req, res) => {
    const productList = await Product.find().populate('category');

    if (!productList) {
        res.status(500).json({success: false, data: null})
    }

    res.send({success: true, data: productList});
});

router.get('/getById/:id', async (req, res) => {
    try {
        const product = await Product.findById(req.params.id).populate('category');

        if (!product) {
            return res.status(400).send({success: false, data: 'Product not found'});
        } 
        return res.send({success: true, data: product});
    } catch (err) {
        return res.status(400).send({success: false, data: err.message});
    }
});

router.get('/getStockCount/', async (req, res) => {
    try {
        const productCount = await Product.countDocuments((count) => count)

        if (!productCount) {
            return res.status(400).send({success: false, data: 'Product not found'});
        } 
        return res.send({success: true, productCount: productCount});
    } catch (err) {
        return res.status(400).send({success: false, data: err.message});
    }
});

router.get('/getFeaturedProducts/', async (req, res) => {
    try {
        const featuredProduct = await Product.find({isFeatured: true});

        if (!featuredProduct) {
            return res.status(400).send({success: false, data: 'Product not found'});
        } 
        return res.send({success: true, productCount: featuredProduct});
    } catch (err) {
        return res.status(400).send({success: false, data: err.message});
    }
});



router.post(`/`, async (req, res) => {

    try {
        const category = await Category.findById(req.body.category);

        if (!category) {
            return res.status(400).send({success: false, data: 'Invalid Category'});
        } else {
            console.log(`dubug: ${category}`);
        }
    } catch {
        return res.status(400).send({success: false, data: 'Invalid Category'});
    }

    var product = new Product({
        name: req.body.name,
        description: req.body.description,
        richDescription: req.body.richDescription,
        image: req.body.image,
        brand: req.body.brand,
        price: req.body.price,
        category: req.body.category,
        countInStock: req.body.countInStock,
        rating: req.body.rating,
        numReviews: req.body.numReviews,
        isFeatured: req.body.isFeatured
    });

    try {
        product = await product.save();

        if (!product) {
            return res.status(500).send({success: false, data: 'The product cannot be created'});
        }

        return res.send({success: true, data: product});
    } catch (err) {
        return res.status(500).send({success: false, data: `debug: ${err.message}`});
    }
});

router.put('/updateById/:id', async (req, res) => {

    if (!mongoose.isValidObjectId(req.params.id)) {
        return res.status(400).send({success: false, data: 'Invalid product id'});
    }
    try {
        const category = await Category.findById(req.body.category);

        if (!category) {
            return res.status(400).send({success: false, data: 'Invalid Category'});
        } else {
            console.log(`dubug: ${category}`);
        }
    } catch {
        return res.status(400).send({success: false, data: 'Invalid Category'});
    }

    try {
        const product = await Product.findByIdAndUpdate(
            req.params.id,
            {
                name: req.body.name,
                description: req.body.description,
                richDescription: req.body.richDescription,
                image: req.body.image,
                brand: req.body.brand,
                price: req.body.price,
                category: req.body.category,
                countInStock: req.body.countInStock,
                rating: req.body.rating,
                numReviews: req.body.numReviews,
                isFeatured: req.body.isFeatured
            },
            { new: true }
        )
    
        if (!product) {
            return response.status(500).send({ success: false,  data: null});
        } 
    
        res.send({ success: true, data: product});
    } catch (err) {
        res.send({ success: false, data: null});
    }
});

router.delete('/deleteById/:id', async (req, res) => {
    if (!mongoose.isValidObjectId(req.params.id)) {
        return res.status(400).send({success: false, data: 'Invalid product id'});
    }
    try {
        const product = await Product.findByIdAndRemove(req.params.id)

        if (product) {
            return res.status(200).json({ success: true, data: 'The product has been deleted' });
        } else {
            return res.status(404).json({ success: false, data: 'The product was not found' });
        }
    } catch(err) {
        return res.status(400).json({ success: false, data: `debug ${err.message}`});
    }
});

router.delete('/deleteAll', async (req, res) => {
    try {
        const products = await Product.deleteMany(function(err, result) {
            if (!err) {
                return res.status(200).json({ success: true, message: 'All products have been deleted' });
            } else {
                return res.status(404).json({ success: false, message: 'Could not delete' });
            }
        })
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message});
    }
})

module.exports = router