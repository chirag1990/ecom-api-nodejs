const {Category} = require('../models/category')
const express = require('express');
const { response } = require('express');
const { json } = require('body-parser');
const router = express.Router();


router.get(`/`, async (req, res) => {
    const categoryList = await Category.find();

    if (!categoryList) {
        res.status(500).json({success: false})
    }

    res.send(categoryList);
});

router.get('/getById/:id', async (req,res) => {
    try {
        const category = await Category.findById(req.params.id);
    
        if (!category) {
            return res.status(500).json({ success: false, data: null });
        }
        return res.send({success: true, data: category});
    } catch {
        return res.status(500).json({ success: false, data: null });
    }
})

router.post('/', async (req, res) => {
    let category = new Category({
        name: req.body.name,
        icon: req.body.icon,
        color: req.body.color
    });

    category = await category.save();

    if (!category) {
        return response.status(404).send({ success: false,  data: null});
    } 

    res.send({ success: true, data: category});
});

router.put('/updateById/:id', async (req, res) => {
    if (!mongoose.isValidObjectId(req.params.id)) {
        return res.status(400).send({success: false, data: 'Invalid category id'});
    }
    try {
        const category = await Category.findByIdAndUpdate(
            req.params.id,
            {
                name: req.body.name,
                icon: req.body.icon,
                color: req.body.color,
            },
            { new: true }
        )
    
        if (!category) {
            return response.status(404).send({ success: false,  data: null});
        } 
    
        res.send({ success: true, data: category});
    } catch (err) {
        res.send({ success: false, data: null});
    }
});

router.delete('/deleteById/:id', async (req, res) => {
    if (!mongoose.isValidObjectId(req.params.id)) {
        return res.status(400).send({success: false, data: 'Invalid category id'});
    }
    try {
        const category = await Category.findByIdAndRemove(req.params.id)

        if (category) {
            return res.status(200).json({ success: true, message: 'The category is deleted' });
        } else {
            return res.status(404).json({ success: false, message: 'The category was not found' });
        }
    } catch(err) {
        return res.status(400).json({ success: false, message: err.message});
    }
});

router.delete('/deleteAll', async (req, res) => {
    try {
        const category = await Category.deleteMany(function(err, result) {
            if (!err) {
                return res.status(200).json({ success: true, message: 'All categories have been deleted' });
            } else {
                return res.status(404).json({ success: false, message: 'Could not delete' });
            }
        })
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message});
    }
})


module.exports = router