const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 3000;
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.set('useFindAndModify', false);

const productsRoutes = require('./routes/products');
const categoriesRoutes = require('./routes/categories');
const ordersRoutes = require('./routes/orders');
const usersRoutes = require('./routes/users');
require('dotenv/config');
const api = process.env.API_URL;

app.use(cors());
app.options('*', cors());

//Middleware
app.use(bodyParser.json());
app.use(morgan('tiny'));

//Routers
app.use(`${api}/products`, productsRoutes);
app.use(`${api}/categories`, categoriesRoutes);
app.use(`${api}/orders`, ordersRoutes);
app.use(`${api}/users`, usersRoutes);


mongoose.connect(process.env.CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(()=> {
    console.log('Database connection is ready...');
})
.catch((err) => {
    console.log(err);
})

app.listen(port, () => {
    console.log('server is running on port ' + port);
});